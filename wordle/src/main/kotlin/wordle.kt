import java.util.*

/*
* para comparar letras tengo que utilizar .get(numero de posicion de la letra)
* ex: palabra.get(0) == solucion.get(0)
*       en le ejemplo comparo la primera palabra
* */


fun main(){
    /*DECLARACION DE VARIABLES*/

    var win: Boolean= false
    val word = arrayOf("acido","bucal","blusa","dulce","coger","comer","estar","lucha","limbo","lamer","nivel","nacer","nueva","salir","senda","pedir","pieza","pulso","bingo","baile").random()

    var lives = 6
    var menu = 0
    val repetedWords: MutableList<String> = mutableListOf("")

    /*Colores*/

    val green_background = "\u001B[42m"
    val grey_bakcground = "\u001B[47m"
    val yellow_background = "\u001B[43m"
    val red_background = "\u001B[41m"
    val red = "\u001B[31m"
    val green = "\u001B[32m"
    val black = "\u001B[30m"
    val reset = "\u001B[0m"

    /*MENU*/

    do {
        println("********************")
        println("****** WORDLE ******")
        println("********************")
        println()
        println()
        println("1. Reglas")
        println("2. Jugar")
        println()
        println()
        val scanner = Scanner(System.`in`)
        println("Introduce una opcion del menu")
        val menu = scanner.nextInt()

        when(menu){
            1 -> {
                println("********************")
                println("****** REGLAS ******")
                println("********************")
                println("$grey_bakcground aaa $reset")
                println()
                println()
                println()
                println("- El jugador tendra que adivinar una palaba de 5 letras, para ello tendra  que ir introduciendo palabas de 5 letras has acertar la palabra ")
                println("- El jugador tendra 6 vidas, por cada intento sin acierto perdera una vida, en caso de perder todas la vidas el jugador habra perdido la partida")
                println("- Las letras de las palabras introducias tendran difernetes colores:")
                println("             - $green_background$black VERDE $reset en caso de que la letra este en misma posicion que en la palabra a adivinar")
                println("             - $yellow_background$black AMARILLO $reset en caso de que la letra esta en la palabra aconseguir pero no en la posicion correct")
                println("             - $grey_bakcground$black GRIS $reset en caso de que la letra no este en la palabra")
                println()
                println()
                println()
                println()
            }
            2 -> {
                println("********************")
                println("* EMPIEZA EL JUEGO *")
                println("********************")
                println()
                println()
                println()
                }
            else -> {
                println("no es una opcion del menu")
            }
        }

    }while (menu !=2)


    /*Juego*/

     while (lives>0 && win == false){
        val scanner = Scanner(System.`in`)
        /*println("Introduce una palabra:")*/
        val imputWord = scanner.next()

        if (imputWord != word && imputWord.length == 5 && imputWord !in repetedWords){
            repetedWords.add(imputWord)
            for (i in 0..4){
                if (imputWord.get(i) == word.get(i)) print("$green_background$black ${imputWord.get(i)} $reset")
                else if (imputWord.get(i) in word) print("$yellow_background$black ${imputWord.get(i)} $reset")
                else print("$grey_bakcground$black ${imputWord.get(i)} $reset")
                }
            lives--
            print("                              Vidas restantes = $lives")
            println()
        }
        else if (imputWord == word) win = true
        else if (imputWord.length > 5) println("La palabra tiene demasiados caracteres")
        else if (imputWord.length < 5) println("La palabra tiene caracteres insuficientes")
        else if (imputWord in repetedWords) println("La palabra ya ha sido introducida")
    }

    /*Resultado del juego*/

    if (win == true) {
        println()
        println()
        println()
        println()
        println("$green*****************************$reset")
        println("$green*  FELICIDADES, HAS GANADO  *$reset")
        println("$green*****************************$reset")
        println()
        println()
        println("La palabra correcta era: $green_background$black $word $reset")
        println()


    }
    else {
        println()
        println()
        println()
        println()
        println("$red*******************************************$reset")
        println("$red*  Te has quedado sin vidas, has perdido  *$reset")
        println("$red*******************************************$reset")
        println()
        println()
        println("La palabra correcta era: $red_background$black $word $reset")
        println()
    }
}
